<?php namespace Flo\VersionPilot\Api\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Flo\VersionPilot\Api\Responders\InvalidEventResponder;

class InvalidEventController extends Controller
{
    /**
     * @var \Flo\VersionPilot\Api\Responders\InvalidEventResponder
     */
    protected $responder;

    /**
     * @param \Flo\VersionPilot\Api\Responders\InvalidEventResponder $responder
     */
    public function __construct(InvalidEventResponder $responder)
    {
        $this->responder = $responder;
    }

    public function __invoke(Request $request)
    {
        return $this->responder->buildResponse($request);
    }
}
