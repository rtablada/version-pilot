<?php namespace Flo\VersionPilot\Api\Controllers;

use Flo\VersionPilot\Api\Responders\DestroyResponder;
use Illuminate\Routing\Controller;
use Flo\VersionPilot\Api\Handlers\DestroyHandler;
use Illuminate\Http\Request;
use Flo\VersionPilot\Api\Responders\InvalidEventResponder;

class DestroyController extends Controller
{
    /**
     * @var \Flo\VersionPilot\Api\Handlers\DestroyHandler
     */
    private $handler;
    /**
     * @var \Flo\VersionPilot\Api\Responders\DestroyResponder
     */
    private $responder;

    /**
     * @param \Flo\VersionPilot\Api\Handlers\DestroyHandler $handler
     * @param \Flo\VersionPilot\Api\Responders\DestroyResponder $responder
     */
    public function __construct(DestroyHandler $handler, DestroyResponder $responder)
    {
        $this->handler = $handler;
        $this->responder = $responder;
    }

    public function __invoke(Request $request)
    {
        $data = $this->handler->handleRequest($request);

        return $this->responder->buildResponse($data);
    }
}
