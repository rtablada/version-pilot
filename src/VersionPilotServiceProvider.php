<?php namespace Flo\VersionPilot;

use ReflectionClass;
use Illuminate\Support\ServiceProvider;

class VersionPilotServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }


    /**
     * Boot Version Pilot
     *
     * @return void
     */
    public function boot()
    {
        $this->package('flo/version-pilot');

        $this->bootCache();
        $this->boootDefaultStorage();

        $this->app['versionpilot.service'] = $this->app->share(function ($app) {
            return new VersionPilot($app['versionpilot.cache'], $app['versionpilot.default']);
        });

        $this->registerAliases();

        $this->app['view']->share('versionPilot', $this->app['versionpilot.service']);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('versionpilot.cache', 'versionpilot.default', 'versionpilot.service');
    }

    /**
     * Guess the package path for the provider.
     *
     * @return string
     */
    public function guessPackagePath()
    {
        $path = (new ReflectionClass($this))->getFileName();

        return realpath(dirname($path));
    }

    protected function bootCache()
    {
        $this->app['versionpilot.cache'] = $this->app->share(function ($app) {
            $cache = new LaravelCacheProvider($app['request'], $app['cache.store']);

            $cache->setVersionQueryParam($app['config']['version-pilot::version_query_param']);

            return $cache;
        });
    }

    protected function boootDefaultStorage()
    {
        $this->app['versionpilot.default'] = $this->app->share(function ($app) {
            return new LaravelDefaultStorageProvider($app['config']);
        });
    }

    protected function registerAliases()
    {
        $this->app->alias('versionpilot.cache', 'Flo\\VersionPilot\\VersionCacheInterface');
        $this->app->alias('versionpilot.default', 'Flo\\VersionPilot\\DefaultStorageInterface');
    }

}
