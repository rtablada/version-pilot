<?php  namespace Flo\VersionPilot;

use Illuminate\Config\Repository;

class LaravelDefaultStorageProvider implements DefaultStorageInterface
{
    /**
     * @var \Illuminate\Config\Repository
     */
    protected $config;

    /**
     * @param \Illuminate\Config\Repository $config
     */
    public function __construct(Repository $config)
    {

        $this->config = $config;
    }

    public function getDefaultForKey($key)
    {
        $data = $this->config->get('version-pilot::apps');

        return ArrayAccess::accessArrayWithDotSyntax($data, $key);
    }
}
