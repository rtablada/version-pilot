<?php  namespace Flo\VersionPilot;

class ArrayAccess
{
    public static function accessArrayWithDotSyntax($array, $key)
    {
        if (!is_array($array)) {
            return null;
        }

        if (strpos($key, '.') === false) {
            if (isset($array[$key])) {
                return $array[$key];
            }

            return null;
        }

        list($parentKey, $childKey) = explode('.', $key, 2);

        if (isset($array[$parentKey])) {
            return static::accessArrayWithDotSyntax($array[$parentKey], $childKey);
        }
    }
}
