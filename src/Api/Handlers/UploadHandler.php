<?php  namespace Flo\VersionPilot\Api\Handlers;

use Flo\VersionPilot\VersionPilot;
use Flo\VersionPilot\Api\Validators\UploadValidator;
use Flo\VersionPilot\Api\Payloads\UploadPayload;
use Flo\VersionPilot\Api\Payloads\ValidationErrorPayload;

class UploadHandler
{
    /**
     * @var \Flo\VersionPilot\VersionPilot
     */
    protected $pilot;
    /**
     * @var \Flo\VersionPilot\Api\Validators\UploadValidator
     */
    protected $validator;

    /**
     * @param \Flo\VersionPilot\VersionPilot $pilot
     * @param \Flo\VersionPilot\Api\Validators\UploadValidator $validator
     */
    public function __construct(VersionPilot $pilot, UploadValidator $validator)
    {
        $this->pilot = $pilot;
        $this->validator = $validator;
    }

    public function handleRequest($request)
    {
        $data = $request->only('app', 'version', 'assets', 'publish');

        if ($this->validator->validate($data)) {
            $uploadedData = $this->pilot->uploadVersion(
                $data['app'],
                $data['version'],
                $data['assets']);

            if ($data['publish']) {
                $this->pilot->publishVersion($data['app'], $data['version']);
            }

            return new UploadPayload(
                $data['app'],
                $data['version'],
                $uploadedData['assets'],
                $uploadedData['availableVersions'],
                $data['publish']);
        } else {
            return new ValidationErrorPayload($this->validator->errors);
        }
    }
}
