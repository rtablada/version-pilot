<?php  namespace Flo\VersionPilot\Api\Handlers;

use Flo\VersionPilot\VersionPilot;
use Flo\VersionPilot\Api\Validators\PublishValidator;
use Flo\VersionPilot\Api\Payloads\PublishPayload;
use Flo\VersionPilot\Api\Payloads\ValidationErrorPayload;

class PublishHandler
{
    /**
     * @var \Flo\VersionPilot\VersionPilot
     */
    protected $pilot;
    /**
     * @var \Flo\VersionPilot\Api\Validators\PublishValidator
     */
    protected $validator;

    /**
     * @param \Flo\VersionPilot\VersionPilot $pilot
     * @param \Flo\VersionPilot\Api\Validators\PublishValidator $validator
     */
    public function __construct(VersionPilot $pilot, PublishValidator $validator)
    {
        $this->pilot = $pilot;
        $this->validator = $validator;
    }

    public function handleRequest($request)
    {
        $data = $request->only('app', 'version');

        if ($this->validator->validate($data)) {
            $uploadedData = $this->pilot->publishVersion(
                $data['app'],
                $data['version']);

            return new PublishPayload($data['app'], $data['version']);
        } else {
            return new ValidationErrorPayload($this->validator->errors);
        }
    }
}
