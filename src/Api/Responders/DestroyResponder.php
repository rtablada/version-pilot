<?php  namespace Flo\VersionPilot\Api\Responders;

use Flo\VersionPilot\Api\Payloads\DestroyPayload;
use Flo\VersionPilot\Api\Payloads\ValidationErrorPayload;
use Illuminate\Http\JsonResponse;

class DestroyResponder implements Responder
{
    public function buildResponse($payload)
    {
        switch (get_class($payload)) {
            case 'Flo\\VersionPilot\\Api\\Payloads\\ValidationErrorPayload':
                return $this->buildValidationFailedResponse($payload);
            case 'Flo\\VersionPilot\\Api\\Payloads\\DestroyPayload':
                return $this->buildDestroySuccessResponse($payload);
        }
    }

    public function buildDestroySuccessResponse(DestroyPayload $payload)
    {
        $data = [
            'status' => 200,
            'message' => 'Version destroyed.',
            'app' => $payload->app,
            'version' => $payload->version,
        ];

        return JsonResponse::create($data, 200);
    }

    public function buildValidationFailedResponse(ValidationErrorPayload $payload)
    {
        $data = [
            'status' => 400,
            'message' => 'Validation failed.',
            'errors' => $payload->errors,
        ];

        return JsonResponse::create($data, 400);
    }
}
