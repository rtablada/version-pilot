<?php  namespace Flo\VersionPilot\Api\Validators;

class PublishValidator extends BaseValidator
{
    protected $rules = [
        'app' => 'required',
        'version' => 'required',
    ];
}
