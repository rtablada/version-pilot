<?php  namespace Flo\VersionPilot\Api\Validators;

class UploadValidator extends BaseValidator
{
    protected $rules = [
        'app' => 'required',
        'version' => 'required',
        'assets' => 'array|required',
        'publish' => 'boolean',
    ];

    protected $messages = [
        'assets.required' => 'The assets field must be a hash of assets associated with the specified version.',
        'assets.array' => 'The assets field must be a hash of assets associated with the specified version.',
    ];
}
