<?php  namespace Flo\VersionPilot\Api\Payloads;

class StatusPayload
{
    public $app;
    public $versions;
    public $master;

    public function __construct($app, $versions, $master)
    {
        $this->app = $app;
        $this->versions = $versions;
        $this->master = $master;
    }
}
