<?php namespace Flo\VersionPilot\Api\Controllers;

use Flo\VersionPilot\Api\Responders\StatusResponder;
use Illuminate\Routing\Controller;
use Flo\VersionPilot\Api\Handlers\StatusHandler;
use Illuminate\Http\Request;
use Flo\VersionPilot\Api\Responders\InvalidEventResponder;

class StatusController extends Controller
{
    /**
     * @var \Flo\VersionPilot\Api\Handlers\StatusStatusHandler
     */
    private $handler;
    /**
     * @var \Flo\VersionPilot\Api\Responders\StatusResponder
     */
    private $responder;

    /**
     * @param \Flo\VersionPilot\Api\Handlers\StatusHandler $handler
     * @param \Flo\VersionPilot\Api\Responders\StatusResponder $responder
     */
    public function __construct(StatusHandler $handler, StatusResponder $responder)
    {
        $this->handler = $handler;
        $this->responder = $responder;
    }

    public function __invoke(Request $request)
    {
        $data = $this->handler->handleRequest($request);

        return $this->responder->buildResponse($data);
    }
}
