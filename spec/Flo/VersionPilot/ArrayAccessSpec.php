<?php

namespace spec\Flo\VersionPilot;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ArrayAccessSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Flo\VersionPilot\ArrayAccess');
    }

    function it_should_return_null_on_invalid_key_from_array()
    {
        $this->accessArrayWithDotSyntax(['x' => 'foo'], 'y')
            ->shouldBeNull();
    }

    function it_should_grab_no_level_key_from_array()
    {
        $this->accessArrayWithDotSyntax(['x' => 'foo'], 'x')
            ->shouldBeEqualTo('foo');
    }

    function it_should_grab_one_level_key_from_array()
    {
        $this->accessArrayWithDotSyntax(['x' => ['y' => 'foo']], 'x.y')
            ->shouldBeEqualTo('foo');
    }

    function it_should_grab_two_level_key_from_array()
    {
        $this->accessArrayWithDotSyntax(['x' => ['y' => ['z' => 'foo']]], 'x.y.z')
            ->shouldBeEqualTo('foo');
    }
}
