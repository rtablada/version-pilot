<?php
namespace Flo\VersionPilot\Api\Responders;

use Flo\VersionPilot\Api\Payloads\UploadPayload;
use Flo\VersionPilot\Api\Payloads\ValidationErrorPayload;

interface Responder
{
    public function buildResponse($payload);
}
