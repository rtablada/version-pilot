<?php  namespace Flo\VersionPilot\Api\Payloads;

class PublishPayload
{
    public $app;
    public $version;

    public function __construct($app, $version)
    {
        $this->app = $app;
        $this->version = $version;
    }
}
