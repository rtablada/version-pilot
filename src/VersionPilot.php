<?php  namespace Flo\VersionPilot;

class VersionPilot
{
    /**
     * @var \Flo\VersionPilot\VersionCacheInterface
     */
    protected $cache;
    /**
     * @var \Flo\VersionPilot\DefaultStorageInterface
     */
    protected $defaults;

    /**
     * @param \Flo\VersionPilot\VersionCacheInterface $cache
     * @param \Flo\VersionPilot\DefaultStorageInterface $defaults
     */
    function __construct(VersionCacheInterface $cache, DefaultStorageInterface $defaults)
    {
        $this->cache = $cache;
        $this->defaults = $defaults;
    }

    public function getAsset($asset)
    {
        list($cacheKey, $assetKey) = explode('.', $asset, 2);

        $data = $this->cache->getVersion($cacheKey);

        $dataUrl = ArrayAccess::accessArrayWithDotSyntax($data, $assetKey);

        if ($dataUrl) {
            return $dataUrl;
        } else {
            return $this->getDefaultAsset($asset);
        }
    }

    public function getDefaultAsset($asset)
    {
        return $this->defaults->getDefaultForKey($asset);
    }

    public function uploadVersion($app, $version, $assets)
    {
        $this->cache->pushVersion($app, $version, $assets);

        $availableVersions = $this->cache->updateAvailableVersions($app, $version);

        return compact('assets', 'availableVersions');
    }

    public function publishVersion($app, $version)
    {
        $this->cache->publishVersion($app, $version);
    }

    public function destroyVersion($app, $version)
    {
        $this->cache->forgetVersion($app, $version);
    }

    public function getAvailableVersions($app)
    {
        $versions = $this->cache->getAvailableVersions($app);

        $master = $this->cache->getMasterVersionNumber($app);

        return compact('versions', 'master');
    }
}
