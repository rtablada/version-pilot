<?php  namespace Flo\VersionPilot\Api\Payloads;

class ValidationErrorPayload
{
    public $errors;

    /**
     * @param $errors
     */
    public function __construct($errors)
    {
        $this->errors = $errors;
    }
}
