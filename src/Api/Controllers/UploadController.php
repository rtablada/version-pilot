<?php namespace Flo\VersionPilot\Api\Controllers;

use Flo\VersionPilot\Api\Responders\UploadResponder;
use Illuminate\Routing\Controller;
use Flo\VersionPilot\Api\Handlers\UploadHandler;
use Illuminate\Http\Request;
use Flo\VersionPilot\Api\Responders\InvalidEventResponder;

class UploadController extends Controller
{
    /**
     * @var \Flo\VersionPilot\Api\Handlers\UploadHandler
     */
    private $handler;
    /**
     * @var \Flo\VersionPilot\Api\Responders\UploadResponder
     */
    private $responder;

    /**
     * @param \Flo\VersionPilot\Api\Handlers\UploadHandler $handler
     * @param \Flo\VersionPilot\Api\Responders\UploadResponder $responder
     */
    public function __construct(UploadHandler $handler, UploadResponder $responder)
    {
        $this->handler = $handler;
        $this->responder = $responder;
    }

    public function __invoke(Request $request)
    {
        $data = $this->handler->handleRequest($request);

        return $this->responder->buildResponse($data);
    }
}
