<?php namespace Flo\VersionPilot\Api\Validators;

use Illuminate\Validation\Factory;

abstract class BaseValidator
{
    public $errors;

    protected $rules = [];

    protected $messages = [];

    /**
     * @var \Illuminate\Validation\Validator
     */
    protected $validator;

    /**
     * @param \Illuminate\Validation\Validator $validator
     */
    public function __construct(Factory $validator)
    {
        $this->validator = $validator;
    }

    public function validate($request)
    {
        $validator = $this->validator->make($request, $this->rules, $this->messages);

        if ($validator->passes()) {
            return true;
        } else {
            $this->errors = $validator->errors();

            return false;
        }
    }
}
