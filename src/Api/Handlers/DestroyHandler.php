<?php  namespace Flo\VersionPilot\Api\Handlers;

use Flo\VersionPilot\VersionPilot;
use Flo\VersionPilot\Api\Validators\DestroyValidator;
use Flo\VersionPilot\Api\Payloads\DestroyPayload;
use Flo\VersionPilot\Api\Payloads\ValidationErrorPayload;

class DestroyHandler
{
    /**
     * @var \Flo\VersionPilot\VersionPilot
     */
    protected $pilot;
    /**
     * @var \Flo\VersionPilot\Api\Validators\DestroyValidator
     */
    protected $validator;

    /**
     * @param \Flo\VersionPilot\VersionPilot $pilot
     * @param \Flo\VersionPilot\Api\Validators\DestroyValidator $validator
     */
    public function __construct(VersionPilot $pilot, DestroyValidator $validator)
    {
        $this->pilot = $pilot;
        $this->validator = $validator;
    }

    public function handleRequest($request)
    {
        $data = $request->only('app', 'version');

        if ($this->validator->validate($data)) {
            $uploadedData = $this->pilot->destroyVersion(
                $data['app'],
                $data['version']);

            return new DestroyPayload($data['app'], $data['version']);
        } else {
            return new ValidationErrorPayload($this->validator->errors);
        }
    }
}
