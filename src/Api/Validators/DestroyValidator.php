<?php  namespace Flo\VersionPilot\Api\Validators;

class DestroyValidator extends BaseValidator
{
    protected $rules = [
        'app' => 'required',
        'version' => 'required',
    ];
}
