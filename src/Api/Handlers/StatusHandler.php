<?php  namespace Flo\VersionPilot\Api\Handlers;

use Flo\VersionPilot\VersionPilot;
use Flo\VersionPilot\Api\Validators\StatusValidator;
use Flo\VersionPilot\Api\Payloads\StatusPayload;
use Flo\VersionPilot\Api\Payloads\ValidationErrorPayload;

class StatusHandler
{
    /**
     * @var \Flo\VersionPilot\VersionPilot
     */
    protected $pilot;
    /**
     * @var \Flo\VersionPilot\Api\Validators\StatusValidator
     */
    protected $validator;

    /**
     * @param \Flo\VersionPilot\VersionPilot $pilot
     * @param \Flo\VersionPilot\Api\Validators\StatusValidator $validator
     */
    public function __construct(VersionPilot $pilot, StatusValidator $validator)
    {
        $this->pilot = $pilot;
        $this->validator = $validator;
    }

    public function handleRequest($request)
    {
        $data = $request->only('app');

        if ($this->validator->validate($data)) {
            $status = $this->pilot->getAvailableVersions($data['app']);

            return new StatusPayload($data['app'], $status['versions'], $status['master']);
        } else {
            return new ValidationErrorPayload($this->validator->errors);
        }
    }
}
