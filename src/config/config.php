<?php

/*
 * Register your apps and defaults for fallback in development
 *
 * For instance:
 * 'apps' => [
 *     'admin' => [
 *         'app-css' => '/css/admin.css'
 *     ]
 * ]
 */
return [
    'apps' => [
    ],

    'version_query_param' => 'app_version',

    'api_route' => 'api/version-pilot',
];
