<?php  namespace Flo\VersionPilot\Api\Payloads;

class UploadPayload
{
    public $app;
    public $version;
    public $assets;
    public $publish;
    public $availableVersions;

    public function __construct($app, $version, $assets, $availableVersions, $publish)
    {
        $this->app = $app;
        $this->version = $version;
        $this->assets = $assets;
        $this->availableVersions = $availableVersions;
        $this->publish = $publish;
    }
}
