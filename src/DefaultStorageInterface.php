<?php  namespace Flo\VersionPilot;

interface DefaultStorageInterface
{
    public function getDefaultForKey($key);
}
