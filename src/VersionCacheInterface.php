<?php namespace Flo\VersionPilot;


interface VersionCacheInterface
{
    public function pushVersion($key, $version, $data);

    public function getVersion($key);

    public function updateAvailableVersions($key, $version);
}
