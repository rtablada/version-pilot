<?php  namespace Flo\VersionPilot\Api\Responders;

use Flo\VersionPilot\Api\Payloads\PublishPayload;
use Flo\VersionPilot\Api\Payloads\ValidationErrorPayload;
use Illuminate\Http\JsonResponse;

class PublishResponder implements Responder
{
    public function buildResponse($payload)
    {
        switch (get_class($payload)) {
            case 'Flo\\VersionPilot\\Api\\Payloads\\ValidationErrorPayload':
                return $this->buildValidationFailedResponse($payload);
            case 'Flo\\VersionPilot\\Api\\Payloads\\PublishPayload':
                return $this->buildPublishSuccessResponse($payload);
        }
    }

    public function buildPublishSuccessResponse(PublishPayload $payload)
    {
        $data = [
            'status' => 200,
            'message' => 'Version updated.',
            'app' => $payload->app,
            'version' => $payload->version,
        ];

        return JsonResponse::create($data, 200);
    }

    public function buildValidationFailedResponse(ValidationErrorPayload $payload)
    {
        $data = [
            'status' => 400,
            'message' => 'Validation failed.',
            'errors' => $payload->errors,
        ];

        return JsonResponse::create($data, 400);
    }
}
