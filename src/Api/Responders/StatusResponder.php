<?php  namespace Flo\VersionPilot\Api\Responders;

use Flo\VersionPilot\Api\Payloads\StatusPayload;
use Flo\VersionPilot\Api\Payloads\ValidationErrorPayload;
use Illuminate\Http\JsonResponse;

class StatusResponder implements Responder
{
    public function buildResponse($payload)
    {
        switch (get_class($payload)) {
            case 'Flo\\VersionPilot\\Api\\Payloads\\ValidationErrorPayload':
                return $this->buildValidationFailedResponse($payload);
            case 'Flo\\VersionPilot\\Api\\Payloads\\StatusPayload':
                return $this->buildStatusSuccessResponse($payload);
        }
    }

    public function buildStatusSuccessResponse(StatusPayload $payload)
    {
        $data = [
            'status' => 200,
            'message' => 'Versions available.',
            'app' => $payload->app,
            'current_version' => $payload->master,
            'versions' => $payload->versions,
        ];

        return JsonResponse::create($data, 200);
    }

    public function buildValidationFailedResponse(ValidationErrorPayload $payload)
    {
        $data = [
            'status' => 400,
            'message' => 'Validation failed.',
            'errors' => $payload->errors,
        ];

        return JsonResponse::create($data, 400);
    }
}
