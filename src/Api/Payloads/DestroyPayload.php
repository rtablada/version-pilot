<?php  namespace Flo\VersionPilot\Api\Payloads;

class DestroyPayload
{
    public $app;
    public $version;

    public function __construct($app, $version)
    {
        $this->app = $app;
        $this->version = $version;
    }
}
