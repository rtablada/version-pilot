<?php  namespace Flo\VersionPilot;

use Illuminate\Cache\Repository;
use Symfony\Component\HttpFoundation\Request;

class LaravelCacheProvider implements VersionCacheInterface
{
    /**
     * @var \Illuminate\Cache\StoreInterface
     */
    protected $cacheStore;

    /**
     * @var string
     */
    protected $versionQueryParam = 'app_version';

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    private $request;

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Illuminate\Cache\Repository|\Illuminate\Cache\StoreInterface $cacheStore
     */
    function __construct(Request $request, Repository $cacheStore)
    {
        $this->cacheStore = $cacheStore;
        $this->request = $request;
    }

    /**
     * Adds version data to the cache
     *
     * @param $key
     * @param $data
     */
    public function pushVersion($key, $version, $data)
    {
        $this->cacheStore->forever("versionpilot:{$key}:{$version}", json_encode($data));
    }

    /**
     * Get version data from the cache
     *
     * @param $key
     * @return array
     */
    public function getVersion($key)
    {
        $version = $this->getVersionNumber($key);

        return json_decode($this->cacheStore->get("versionpilot:{$key}:{$version}"), true);
    }

    public function setVersionQueryParam($param)
    {
        $this->versionQueryParam = $param;
    }

    public function publishVersion($key, $version)
    {
        $this->cacheStore->forever("versionpilot:{$key}:master", json_encode($version));
    }

    public function forgetVersion($key, $version)
    {
        $this->cacheStore->forget("versionpilot:{$key}:{$version}");
    }

    protected function getVersionNumber($key)
    {
        $queryVersion = $this->request->get($this->versionQueryParam);

        if ($queryVersion) {
            return $queryVersion;
        }

        $headerVersion = $this->request->header('X-Version-Pilot-Version') ?: $this->getMasterVersionNumber($key);

        return $headerVersion ?: 'master';
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getMasterVersionNumber($key)
    {
        return json_decode($this->cacheStore->get("versionpilot:{$key}:master"));
    }


    public function updateAvailableVersions($key, $version)
    {
        $currentVersions = $this->getAvailableVersions($key);

        if (! in_array($version, $currentVersions)) {
            $currentVersions[] = $version;

            $this->cacheStore->forever("versionpilot:{$key}", json_encode($currentVersions));
        }

        return $currentVersions;
    }

    /**
     * @param $key
     * @return array
     */
    public function getAvailableVersions($key)
    {
        $currentVersions = json_decode($this->cacheStore->get("versionpilot:{$key}"), true) ?: [];

        return $currentVersions;
    }
}
