<?php

namespace spec\Flo\VersionPilot;

use Flo\VersionPilot\DefaultStorageInterface;
use Flo\VersionPilot\VersionCacheInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class VersionPilotSpec extends ObjectBehavior
{

    function let(VersionCacheInterface $cache, DefaultStorageInterface $storage)
    {
        $this->beConstructedWith($cache, $storage);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Flo\VersionPilot\VersionPilot');
    }

    function it_should_grab_an_asset(VersionCacheInterface $cache)
    {
        $cache->getVersion('app')
            ->shouldBeCalled()
            ->willReturn(['app-css' => 'public/app.css']);

        $this->getAsset('app.app-css')
            ->shouldBeEqualTo('public/app.css');
    }

    function it_should_grab_an_asset_default_if_cache_is_null(VersionCacheInterface $cache, DefaultStorageInterface $storage)
    {
        $cache->getVersion('app')
            ->shouldBeCalled()
            ->willReturn(null);

        $storage->getDefaultForKey('app.app-css')
            ->shouldBeCalled()
            ->willReturn('public/app.css');

        $this->getAsset('app.app-css')
            ->shouldBeEqualTo('public/app.css');
    }
}
