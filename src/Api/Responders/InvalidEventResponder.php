<?php  namespace Flo\VersionPilot\Api\Responders;

use Illuminate\Http\JsonResponse;

class InvalidEventResponder implements Responder
{
    public function buildResponse($request)
    {
        $data = [
            'status' => 406,
            'message' => 'Invalid event passed in header for Version-Pilot.',
            'url' => $request->url(),
            'valid_events' => [
                'update',
                'publish',
                'destroy',
                'status',
            ],
        ];

        return JsonResponse::create($data, 406);
    }
}
