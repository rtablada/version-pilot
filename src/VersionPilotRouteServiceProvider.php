<?php namespace Flo\VersionPilot;

use ReflectionClass;
use Illuminate\Support\ServiceProvider;

class VersionPilotRouteServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }


    /**
     * Boot Version Pilot
     *
     * @return void
     */
    public function boot()
    {
        $router = $this->app['router'];
        $config = $this->app['config'];

        $this->registerRoute($router, $config);
    }

    protected function registerRoute($router, $config)
    {
        $routeName = $config->get('version-pilot::api_route');

        $router->post($routeName, ['uses' => 'Flo\\VersionPilot\\Api\\Controllers\\FrontController@__invoke']);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

}
