<?php namespace Flo\VersionPilot\Api\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    protected $request;

    protected $app;

    public function __construct(Application $app, Request $request)
    {
        $this->app = $app;
        $this->request = $request;
    }

    public function __invoke()
    {
        $controller = $this->getController();

        return $controller->__invoke($this->request);
    }

    protected function getController()
    {
        switch ($this->request->header('X-Versionpilot-Event')) {
            case 'upload':
                return $this->app->make('Flo\\VersionPilot\\Api\\Controllers\\UploadController');
                break;
            case 'publish':
                return $this->app->make('Flo\\VersionPilot\\Api\\Controllers\\PublishController');
                break;
            case 'destroy':
                return $this->app->make('Flo\\VersionPilot\\Api\\Controllers\\DestroyController');
                break;
            case 'status':
                return $this->app->make('Flo\\VersionPilot\\Api\\Controllers\\StatusController');
                break;
            default:
                return $this->app->make('Flo\\VersionPilot\\Api\\Controllers\\InvalidEventController');
                break;
        }
    }
}
