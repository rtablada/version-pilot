<?php namespace Flo\VersionPilot\Api\Controllers;

use Flo\VersionPilot\Api\Responders\PublishResponder;
use Illuminate\Routing\Controller;
use Flo\VersionPilot\Api\Handlers\PublishHandler;
use Illuminate\Http\Request;
use Flo\VersionPilot\Api\Responders\InvalidEventResponder;

class PublishController extends Controller
{
    /**
     * @var \Flo\VersionPilot\Api\Handlers\PublishHandler
     */
    private $handler;
    /**
     * @var \Flo\VersionPilot\Api\Responders\PublishResponder
     */
    private $responder;

    /**
     * @param \Flo\VersionPilot\Api\Handlers\PublishHandler $handler
     * @param \Flo\VersionPilot\Api\Responders\PublishResponder $responder
     */
    public function __construct(PublishHandler $handler, PublishResponder $responder)
    {
        $this->handler = $handler;
        $this->responder = $responder;
    }

    public function __invoke(Request $request)
    {
        $data = $this->handler->handleRequest($request);

        return $this->responder->buildResponse($data);
    }
}
